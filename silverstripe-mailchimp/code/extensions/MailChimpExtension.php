<?php

class MailChimpExtension extends DataExtension
{
    /**
     * @var array
     */
    private static $allowed_actions = [
        'MailChimpForm'
    ];

    public function MailChimpForm()
    {
        $config = SiteConfig::current_site_config();

        $fields = FieldList::create();

        $requiredFields = ['Email'];

        if($config->MailChimpFirstNameRequired) {
            $fields->add(TextField::create('FirstName', 'First name'));
            $requiredFields[] = 'FirstName';
        }

        if($config->MailChimpLastNameRequired) {
            $fields->add(TextField::create('LastName', 'Last name'));
            $requiredFields[] = 'LastName';
        }

        $fields->add(EmailField::create('Email', 'Email'));

        return Form::create(
            $this->owner,
            'MailChimpForm',
            $fields,
            FieldList::create(FormAction::create('MailChimpFormSubmission', 'Submit')),
            RequiredFields::create($requiredFields)
        );
    }

    /**
     * @param array $data
     * @param Form $form
     */
    public function MailChimpFormSubmission($data, $form)
    {
        $config = SiteConfig::current_site_config();

        try {
            // connect to the api
            $mailChimp = new \DrewM\MailChimp\MailChimp($config->MailChimpAPIKey);

            // add the subscriber
            $result = $mailChimp->post('lists/' . $config->MailChimpListID . '/members', [
                'status'        => 'pending',
                'email_address' => $data['Email'],
                'merge_fields' => [
                    'FNAME'         => $data['FirstName'],
                    'LNAME'         => $data['LastName'],
                ],
            ]);
        } catch (Exception $e) {
            SS_Log::log($e->getMessages(), SS_Log::ERR);
        }
    }
}

