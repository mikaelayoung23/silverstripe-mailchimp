<?php

class MailChimpSiteConfigExtension extends DataExtension
{
    /**
     * @var array
     */
    private static $db = [
        'MailChimpListID' => 'varchar(255)',
        'MailChimpAPIKey' => 'varchar(255)',

        'MailChimpFirstNameRequired' => 'boolean',
        'MailChimpLastNameRequired' => 'boolean',
    ];

    public function updateCMSFields(FieldList $fields)
    {
        $fields->addFieldsToTab(
            'Root.MailChimp',
            [
                TextField::create('MailChimpListID', 'MailChimp List ID'),
                TextField::create('MailChimpAPIKey', 'MailChimp API Key'),

                CheckboxField::create('MailChimpFirstNameRequired', 'Require first name'),
                CheckboxField::create('MailChimpLastNameRequired', 'Require last name'),
            ]
        );
    }
}
